# brickplot  <img src='brickplot_sticker.png' align="right" height="160" />

R Package to Plot Distributions of Discrete Variables

[Michael Lee Wood](https://www.michaelleewood.com) and  [Dustin S. Stoltz](https://www.dustinstoltz.com)



## Installing

Install and load the `brickplot` package from GitLab:
```r
  # install.packages("devtools")
  devtools::install_gitlab("woodstoltz/brickplot")
  library(brickplot)

```


## Getting Started
 
The package has two functions `brick_framer` and `brick_plot`. The first function will build a dataset in the right format for plotting. The user can manually feed this into ggplot or input it directly to the `brick_plot` function.

Before heading to the framer, the user will need to make sure that the discrete "categorical" variable (or cat_var) is a factor with levels in an appropriate order. This is commonly a likert-like survey question, such as Strongly Agree to Strongly Disagree. 

For the following examples, we use [Kieran Healy](https://kieranhealy.org/blog/archives/2019/10/10/back-in-the-gssr/)'s `gssr` package to input all the surveys from the General Social Survey.

```r
  devtools::install_github("kjhealy/gssr")
  library(gssr)
  
  library(tidyverse)
  library(data.table)
  
  data(gss_all)
  dat <- gss_all %>% as_factor() %>% as.data.table()
  
 ```
We're going to use the example from [Wood (2019](https://journals.sagepub.com/doi/full/10.1177/2378023119900064) on religious attendance, or the "attend" variable in the GSS. If you inspect attend, you'll see "junk" or unordered categories in the variable ("DK,NA") which we'll remove now for the example, but we will demonstrate below how you can remove these unneeded categories with `brick_framer` (and in a future release you will be able to plot them separately). In the preprocessing below, we must also make sure that the levels of the new factor are in the correct order, in this case from NEVER to MORE THAN ONCE A WEEK.
  

```r

    dat[, r.attend  := car::recode(factor(attend), 'c("DK,NA")=NA') ]
    dat[, r.attend   := factor(r.attend, levels(r.attend )[c(4,2,6,1,7,9,3,8,5)])]
  
```

The next step is to create the dataframe for the plot using `brick_framer`, which just requires a "cat\_var" for the discrete variable, and a grouping variable or "group\_var" by which to "stack" the distributions. This will most likely be over time, but can also be something like a demographic variable, such as social class.  In the example, we have grouped religious attendance by year. If no "group\_var" is specified, the plot will be just one layer for the full sample.

```r

    df.r <- brick_framer(
                        df = dat,
                        group_var = "year", 
                        cat_var = "r.attend", 
                        ) 
```
We've made it so plotting is very simple with just default options. Feed the new dataframe produced by `brick_framer` to `brick_plot` and (if you want) specify titles: 

```r
                        
    brick_plot(df = df.r,
              title = "Religious Attendance in the United States", 
              caption ="Source: General Social Survey 1972-2018", 
              ytitle ="Year"
              )

```

Run the above code and you should see the figure below:

<img align="middle" src="figures/figure1_GSS_religious_attendance.png" width="650" height="400">



That's it! There are several more options you can specify, detailed below.


## Brick Framer Function

The `brick_framer` takes care of wrangling the data into the appropriate format where, among other things, each category variable by group variable subset has a proportion (like a crosstabs). You can feed the resulting dataset directly into ggplot if you'd like more control over the plotting. The function also comes with a few additional options detailed below.

### Extra Categories 

In the preprocessing, you can remove any unneeded categories in the variable, such as "Don't Know" or "Other." You can also specify these as a string for the `omit_cats=""` option. By default, these extra categories are included when the function calculates proportions. However,  if you would like to exclude them from the calculation, just set `count_omitted = FALSE` and run `brick_framer` as usual.

### Facet Plots

If you would like to split the plots by another category, say "Gender", you can simply supply the name of the facet variable to `facet_var = "sex"` and the proportions will be appropriately calculated as if the sample is divided by this facet category (and it will be automatically detected and plotted by `brick_plot`)

### Colors

<span style="background-color: #FFFF00">Marked text</span>

The default colors are c("#ca5800", "#f5f5f5","#0a4c6a")`, but you can specify any three color using hex codes. We rely heavily on the (well-researched) color schemes used by the [Urban Institute](http://urbaninstitute.github.io/graphics-styleguide/#color). The middle color should be lighter than the ends.


## Brick Plot Function

As discussed in the example, you can set `title=""`, `subtitle=""`, `caption=""`, `xtitle=""`, `ytitle=""`. Additionally, you can set `value_labels = TRUE` to print the proportions on the bars, and `legend = FALSE` to remove the legend.

Here's a final example using the `facet_var = "gender"` and setting `value_labels = TRUE`.

```r

        
    # First, recode the years into fewer categories using respective Presidential election year:
    
    dat[, elec.years  := car::recode(year, ' c("1972","1973","1974","1975","1976")=1972; 
                                             c("1977","1978","1979","1980")=1976; 
                                             c("1981","1982","1983","1984")=1980; 
                                             c("1985","1986","1987","1988")=1984; 
                                             c("1989","1990","1991","1992")=1988; 
                                             c("1993","1994","1995","1996")=1992; 
                                             c("1997","1998","1999","2000")=1996; 
                                             c("2001","2002","2003","2004")=2000; 
                                             c("2005","2006","2007","2008")=2004; 
                                             c("2009","2010","2011","2012")=2008; 
                                             c("2013","2014","2015","2016")=2012; 
                                             c("2017","2018")=2016; 
                                             ')  ]


    dat %>%
            mutate(gender=recode(sex, 
                         "MALE"="MEN",
                         "FEMALE"="WOMEN") ) %>%
                        droplevels() %>%
            brick_framer(facet_var = "gender",
                         group_var = "elec.years",
                         cat_var = "natfare",
                         colors = c("#003366", "#B2BECC","#8B1A1A")
                        ) %>%
            brick_plot(title = "The National Government Spends ______ on Welfare", 
                       caption = "Source: General Social Survey 1972-2018", 
                       ytitle = "Election Year", 
                       value_labels = TRUE            
                        )
```

Run the above code, and you will get this facet plot:

<img align="middle" src="figures/Figure2_spending_welfare_by_gender.png" width="650" height="400">



<!-- |  Defaults    |       |
|:---:|:---|
|`value_labels = FALSE` | setting to `TRUE` will print the proportions on the bars |
|`legend = TRUE` | setting to `FALSE` will remove the legend | -->
<!-- | `plot_extra = FALSE` | setting to `TRUE` will plot extra categories. Note: A string must be supplied to `extra_cats` and `plot_extra` set to `TRUE` in `brick_framer` | -->
<!-- |`facet_var = FALSE` | setting to `TRUE` create a facet plot.  Note: A variable must be supplied to `facet_var = ""` in `brick_framer`| -->





# THE END

